/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportstudents;

import java.text.ParseException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


/**
 *
 * @author SAOM
 */
public class Student
{
    private String name, lastName, code, gen, identificationCard, birthDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    public String getIdentificationCard() {
        return identificationCard;
    }

    public void setIdentificationCard(String identificationCard) {
        this.identificationCard = identificationCard;
    }
    
    public String getBirthDate(){
        return birthDate;
    }
    
    public void setBirthDate(String birthDate){
        this.birthDate = birthDate;
    }

   
    public Student()
    {
        name = "";
        lastName = "";
        code = "";
        gen = "";
        birthDate = "";
        identificationCard = "";
                
    }
    
    
    public void generateCode(int a)
    {
        String cod, aux ="";
        int opc = 2;
        String aux2, add="";
        aux2 = getBirthDate();
        
        for(int i=4;i<aux2.length();i++)
        {
            
            add = add + birthDate.charAt(i);
            
        }
        for(int i=0;i<opc;i++)
        {
            aux = aux + lastName.toLowerCase().charAt(i);
            
        }
        cod = add+aux+String.valueOf(a);
        code = cod;
    }
    
    public  boolean validIdentificationC(String x)
    {
        int add = 0;
            if(x.length()==9)
            {
                System.out.println("INGRESE UNA CEDULA DE 10 DÍGITOS");
                return false;
            }
            else
            {
                int a[]=new int [x.length()/2];
                int b[]=new int [(x.length()/2)];
                int c=0;
                int d=1;
                for (int i = 0; i < x.length()/2; i++)
                {
                    a[i]=Integer.parseInt(String.valueOf(x.charAt(c)));
                    c=c+2;
                    if (i < (x.length()/2)-1)
                    {
                        b[i]=Integer.parseInt(String.valueOf(x.charAt(d)));
                        d=d+2;
                    }
                }
    
                for (int i = 0; i < a.length; i++)
                {
                    a[i]=a[i]*2;
                    if (a[i] >9)
                    {
                        a[i]=a[i]-9;
                    }
                    add=add+a[i]+b[i];
                }    
                int aux=add/10;
                int dec=(aux+1)*10;
                if ((dec - add) == Integer.parseInt(String.valueOf(x.charAt(x.length()-1))))
                    return true;
                else
                if(add%10==0 && x.charAt(x.length()-1)=='0')
                {
                    return true;
                }
                else
                {
                    System.out.println("CEDULA INVALIDA INGRESE UNA CEDULA VALIDA PORFAVOR");
                    return false;
                }
       
    }
    }
    
    public boolean validateDate(String bD)
    {
       try
       {
            DateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
            formatDate.setLenient(false);
            formatDate.parse(bD);
       }catch(ParseException e)
       {
           return false;
       }
       return true;
    }
    
}
    
  