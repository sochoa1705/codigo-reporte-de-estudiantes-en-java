/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportstudents;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author SAOM
 */
public class ReportStudents 
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
         Scanner input = new Scanner(System.in);
         Scanner input2 = new Scanner(System.in);
        ArrayList<Student> students= new ArrayList<Student>();
        Student aux;
        int opc, i = 0, choice;
        String opc2;
        boolean aux2, aux3;
       do
       {
            System.out.println("*****Bienvenido al sistema de matriculación*****");
            System.out.println("Escoga una accion a realizar:");
            System.out.println("1.- Inscribir Alumno.");
            System.out.println("2.- Mostrar Reporte de Busqueda.");
            System.out.println("3.- Salir.");
            opc = input.nextInt();
            switch(opc)
            {
                case 1:
                    do
                    {
                        aux = new Student();
                        System.out.println("Ingrese el nombre del estudiante "+(i+1)+":");
                        aux.setName(input.next());
                        System.out.println("Ingrese el apellido del estudiante "+(i+1)+":");
                        aux.setLastName(input.next());
                        do
                        {
                            System.out.println("Ingrese la fecha de nacimiento del estudiante (dd/MM/yyyy):");
                            aux.setBirthDate(input.next());
                            aux2 = aux.validateDate(aux.getBirthDate());
                            if(aux2 != true)
                            {
                                System.out.println("FECHA NO VALIDA, PORFAVOR VUELVA A INGRESAR LA FECHA");
                            }
                        }while(aux2!=true);
                        do
                        {
                            System.out.println("Ingrese la cedula de identidad del estudiante:");
                            opc2 = input.next();
                            aux.setIdentificationCard(opc2);
                            aux3 = aux.validIdentificationC(opc2);
                            
                        }while(aux3!= true);
                        aux.generateCode(i);
                        students.add(aux);
                        System.out.print("DESEA INGRESAR UN NUEVO ALUMNO\n1. SI\n2. NO\n");
                        choice=input.nextInt();
                        i++;
                    }while(choice==1);
                    
                    break;
                
                case 2:
                    Registry book = new Registry(students);
                    System.out.println("Ingrese el numero");
                    book.printReport(input.next());
                    break;
                case 3:
                    break;   
            }
       }while(opc!=3);
   
    }
}
   
   

