/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportstudents;

import java.util.ArrayList;

/**
 *
 * @author SAOM
 */
public class Registry 
{
    private ArrayList<Student> students = new ArrayList<Student>();
    
    public Registry(ArrayList a)
    {
        students = a;
    }
    
    public void printReport(String c)
    {
        
        for(int i=0; i<students.size();i++)
        {
            if(students.get(i).getIdentificationCard().matches("[0-9]{2}[0-9]{2}.*")) 
            {
                System.out.println("Estudiantes registrados");
                System.out.println("Estudiante "+(i+1));
                System.out.println("Nombre: "+students.get(i).getName()+" "+students.get(i).getLastName());
                System.out.println("Fecha de Nacimiento: "+students.get(i).getBirthDate());
                System.out.println("Cèdula de identidad: "+students.get(i).getIdentificationCard());
                System.out.println("Código: "+students.get(i).getCode());
                System.out.println("----------------------------------------------------------------------");
                
            }
        }
    }
}
